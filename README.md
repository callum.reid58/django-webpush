﻿
# Django-WebPush

A simple package to integrate [Web Push Notifications](https://developer.mozilla.org/en/docs/Web/API/Push_API) into your Django Application.

---

# Requirements

* Python (3.8)
* Django (4.0)

# Installation

Install using `pip`...

```bash
pip install git+https://gitlab.com/callum.reid58/django-webpush
```

Add `'webpush'` to your `INSTALLED_APPS` setting.

```python
INSTALL_APPS = [
    ...
    'webpush',
]
```

# Configuration

Install `py-vapid` to generate the necessary public and private keys.

```bash
pip install py-vapid
```

Create a `claims.json` file.

```bash
{
    "sub": "mailto: development@example.com"
}
```

Generate `private_key.pem` file.

```bash
vapid --sign claims.json

Generating private_key.pem
Generating public_key.pem
```

**Note:** You can safely ignore all errors after these pem files are generated.

Generate `applicationServerKey` (This will be required by your frontend application).

```bash
vapid --applicationServerKey

Application Server Key = BHq-9HOKl5wREyhRu-VNZwKdmiqE7LoS7-VDuMfhqi4SoI28W1fzeTI7os4KctFeEZgtV2oso3v390qoJfxsfto
```

**Note:** The above command must be run from the same file path as the previous command or additional `private_key.pem` and `public_key.pem` files will be generated.

**Note:** `py-vapid` is no longer required after completing these steps and can be safely removed.

Update your `WEBPUSH` settings within your `settings.py` module.

```python
WEBPUSH = {
    'PRIVATE_KEY': '/path/to/your/private_key.pem',
	'CLAIMS': {
        'sub': 'mailto: development@example.com'
    }
}
```

**Note:** The contents of `CLAIMS` should be an exact match to your `claims.json` file.

Add the `webpush` urls within your `urls.py` module.

```python
from django.urls import include, path
from webpush.api.routers import router

urlpatterns = [
    ...
    path(r'', include(router.urls)),
]
```