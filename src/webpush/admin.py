# Import django packages
from django.conf import settings
from django.contrib import admin, messages
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

# Import external packages
import json
from pywebpush import WebPushException

# Import my packages
from webpush.forms import SendMessageForm
from webpush.models import WebPushDevice


User = settings.AUTH_USER_MODEL


@admin.register(WebPushDevice)
class WebPushDeviceAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'user', 'active', 'date_created', )
    list_filter = ('active', )
    actions = ('send_message', 'enable', 'disable', )
    raw_id_fields = ('user', )

    if hasattr(User, 'USERNAME_FIELD'):
        search_fields = ('name', 'user__%s' % (User.USERNAME_FIELD), )
    else:
        search_fields = ('name', )

    @admin.action(description=_('Send message'))
    def send_message(self, request, queryset):
        if 'submit' in request.POST:
            head = request.POST['head']
            body = request.POST['body']
            url = request.POST['url']
            icon = request.POST['icon']
            badge = request.POST['badge']
            bulk_send = request.POST.get('bulk_send', False)
            
            data = json.dumps({
                'head': head,
                'body': body,
                'url': url,
                'icon': icon,
                'badge': badge,
            })

            results = []
            errors = []

            if bulk_send:
                try:
                    results = queryset.send_message(data)
                    msg = _(f'All messages were sent: {results}')
                    self.message_user(request, msg)
                except WebPushException as e:
                    self.message_user(
                        request,
                        f'Error: {e}',
                        messages.ERROR,
                    )
            else:
                for device in queryset:
                    try:
                        result = device.send_message(data)
                        results.append(result)
                    except WebPushException as e:
                        errors.append({ 'device': device, 'error': e })
                if results:
                    self.message_user(
                        request,
                        'All notifications were successfully sent' if not errors else 'Some notifications were successfully sent',
                        messages.SUCCESS
                    )
                if errors:
                    self.message_user(
                        request,
                        _(mark_safe(f'{len(errors)} notifications failed to send<br/><br/>' + '<br/>'.join([f'{e["device"]}: {e["error"]}<br/>' for e in errors]))),
                        messages.ERROR,
                    )
            
            return HttpResponseRedirect(request.get_full_path())

        form = SendMessageForm(
            initial={
                '_selected_action': queryset.values_list('id', flat=True)
            }
        )

        return render(
            request,
            'admin/webpush/webpushdevice/webpushdevice_send_message.html',
            context={
                'title': 'Send Message',
                'site_header': admin.site.site_header,
                'site_url': admin.site.site_url,
                'has_permission': admin.site.has_permission(request),
                'form': form,
                'queryset': queryset,
            }
        )
    
    @admin.action(description=_('Enable selected devices'))
    def enable(self, request, queryset):
        queryset.update(active=True)

    @admin.action(description=_('Disable selected devices'))
    def disable(self, request, queryset):
        queryset.update(active=False)