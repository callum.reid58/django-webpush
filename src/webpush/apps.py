# Import django packages
from django.apps import AppConfig


class WebPushConfig(AppConfig):
    name = 'webpush'
    verbose_name = "Django WebPush"
    
    def ready(self):
        from webpush.checks import required_settings_system_check
        return super().ready()