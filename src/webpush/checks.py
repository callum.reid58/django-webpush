# Import django packages
from django.core.checks import Tags, Warning, register

# Import my packages
from webpush.settings import webpush_settings


@register(Tags.compatibility)
def required_settings_system_check(app_configs, **kwargs):
    errors = []
    if not webpush_settings.PRIVATE_KEY:
        errors.append(
            Warning(
                'You have not specified a PRIVATE_KEY webpush setting.',
                hint='The default for PRIVATE_KEY is None.',
                id='webpush.W001'
            )
        )
    if not webpush_settings.CLAIMS:
        errors.append(
            Warning(
                'You have not specified a CLAIMS webpush setting.',
                hint='The default for CLAIMS is None.',
                id='webpush.W002'
            )
        )
    return errors
