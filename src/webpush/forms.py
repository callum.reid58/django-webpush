# Import django packages
from django.core.exceptions import ValidationError
from django.forms import (
    Form,
    BooleanField,
    CharField,
    URLField,
    MultipleHiddenInput,
)
from django.utils.translation import gettext_lazy as _


class SendMessageForm(Form):
    _selected_action = CharField(
        widget=MultipleHiddenInput,
    )
    head = CharField(
        required=True,
    )
    body = CharField(
        required=True,
    )
    url = URLField(
        required=False,
    )
    icon = URLField(
        required=False,
    )
    badge = URLField(
        required=False,
    )
    bulk_send = BooleanField(
        required=False,
    )

    # def clean(self):
    #     cleaned_data = super().clean()
    #     head = cleaned_data.get('head')
    #     body = cleaned_data.get('body')
    #     head, body, url, icon, badge
    #     data = json.dumps({
    #                 'head': 'Test notification head',
    #                 'body': 'Test notification body'
    #             })
    #     date_from = cleaned_data.get('date_from')
    #     date_to = cleaned_data.get('date_to')

    #     if date_from and date_to and date_from > date_to:
    #         self.add_error(
    #             'date_to',
    #             ValidationError(
    #                 _('Invalid date range: "Date to" must be after "Date from"'),
    #                 code='invalid_date_range'
    #             )
    #         )
