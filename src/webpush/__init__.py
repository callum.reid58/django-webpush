import django

__title__ = 'Django WebPush'
__version__ = '0.0.2'
__author__ = 'Callum Reid'
__license__ = 'GNU GPLv3'
# __copyright__ = 'Copyright 2011-2019 Encode OSS Ltd'

# Version synonym
VERSION = __version__


if django.VERSION < (3, 2):
    default_app_config = 'webpush.apps.WebPushConfig'