# Import django pacakges
from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

# Import external packages
from pywebpush import WebPushException, webpush

# Import my packages
from webpush.settings import webpush_settings


class Device(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name=_('Name'),
        blank=True,
        null=True,
    )
    active = models.BooleanField(
        verbose_name=_('Is active'),
        default=True,
        help_text=_('Inactive devices will not be sent notifications'),
    )
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    date_created = models.DateTimeField(
        verbose_name=_('Creation date'),
        auto_now_add=True,
        null=True,
    )
    application_id = models.CharField(
        max_length=64,
        verbose_name=_('Application ID'),
        help_text=_(
            'Opaque application identity, should be filled in for multiple key/certificate access'
        ),
        blank=True,
        null=True,
    )

    class Meta:
        abstract = True

    def __str__(self):
        return (
            self.name or
            f'{self.__class__.__name__} for {self.user or "unknown user"}'
        )


class WebPushDeviceManager(models.Manager):
    def get_queryset(self):
        return WebPushDeviceQuerySet(self.model)


class WebPushDeviceQuerySet(models.query.QuerySet):
    def send_message(self, message, **kwargs):
        devices = self.filter(active=True).order_by('application_id').distinct()

        results = []
        for device in devices:
            results.append(device.send_message(message))

        return results


class WebPushDevice(Device):
    p256dh = models.CharField(
        verbose_name=_('User public encryption key'),
        max_length=88,
    )
    auth = models.CharField(
        verbose_name=_('User auth secret'),
        max_length=24,
    )
    endpoint = models.URLField(
        verbose_name=_('Endpoint'),
        max_length=512,
    )

    objects = WebPushDeviceManager()

    class Meta:
        verbose_name = _('WebPush device')
        verbose_name_plural = _('WebPush devices')

    def get_subscription_info(self):
        return {
            'endpoint': self.endpoint,
            'keys': {
                'auth': self.auth,
                'p256dh': self.p256dh,
            }
        }

    def send_message(self, message, **kwargs):
        subscription_info = self.get_subscription_info()

        try:
            response = webpush(
                subscription_info=subscription_info,
                data=message,
                vapid_private_key=webpush_settings.PRIVATE_KEY,
                vapid_claims=webpush_settings.CLAIMS.copy(),
                **kwargs
            )
            return response
        except WebPushException as e:
            try:
                # If the subscription is expired, delete it.
                if e.response.status_code == 410:
                    self.delete()
                else:
                    # Its other type of exception!
                    raise e
            except AttributeError:
                raise e