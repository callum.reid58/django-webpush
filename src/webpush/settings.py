# Import django packages
from django.conf import settings
from django.test.signals import setting_changed

DEFAULTS = {
    'PRIVATE_KEY': None,
    'CLAIMS': None,
}


class WebPushSettings:
    """
    A settings object that allows WebPush settings to be accessed as
    properties. For example:
        from rest_framework.settings import webpush_settings
        print(webpush_settings.PRIVATE_KEY)
    Any setting with string import paths will be automatically resolved
    and return the class, rather than the string literal.
    Note:
    This is an internal class that is only compatible with settings namespaced
    under the WEBPUSH name. It is not intended to be used by 3rd-party
    apps, and test helpers like `override_settings` may not work as expected.
    """
    def __init__(self, user_settings=None, defaults=None):
        if user_settings:
            self._user_settings = self.__check_user_settings(user_settings)
        self.defaults = defaults or DEFAULTS
        self._cached_attrs = set()

    @property
    def user_settings(self):
        if not hasattr(self, '_user_settings'):
            self._user_settings = getattr(settings, 'WEBPUSH', {})
        return self._user_settings

    def __getattr__(self, attr):
        if attr not in self.defaults:
            raise AttributeError("Invalid WebPush setting: '%s'" % attr)

        try:
            # Check if present in user settings
            val = self.user_settings[attr]
        except KeyError:
            # Fall back to defaults
            val = self.defaults[attr]

        # Cache the result
        self._cached_attrs.add(attr)
        setattr(self, attr, val)
        return val

    def reload(self):
        for attr in self._cached_attrs:
            delattr(self, attr)
        self._cached_attrs.clear()
        if hasattr(self, '_user_settings'):
            delattr(self, '_user_settings')


webpush_settings = WebPushSettings(None, DEFAULTS)


def reload_webpush_settings(*args, **kwargs):
    setting = kwargs['setting']
    if setting == 'WEBPUSH':
        webpush_settings.reload()


setting_changed.connect(reload_webpush_settings)
