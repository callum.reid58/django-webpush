# Import external packages
from rest_framework.serializers import ModelSerializer

# Import my packages
from webpush.models import WebPushDevice


class DeviceSerializerMixin(ModelSerializer):
    class Meta:
        fields = (
            'id',
            'name',
            'application_id',
            'active',
            'date_created',
        )
        read_only_fields = ('date_created', )
        extra_kwargs = {
            'active': {
                'default': True
            }
        }


class WebPushDeviceSerializer(ModelSerializer):
    class Meta(DeviceSerializerMixin.Meta):
        model = WebPushDevice
        fields = (
            'id',
            'name',
            'active',
            'date_created',
            'p256dh',
            'auth',
            'endpoint',
            'application_id',
        )