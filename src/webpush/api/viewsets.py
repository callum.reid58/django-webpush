# Import django packages
from django.http.response import Http404

# Import external packages
from rest_framework.parsers import JSONParser
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

# Import my packages
from webpush.models import WebPushDevice
from webpush.api.permissions import (
    IsOwner,
)
from webpush.api.serializers import (
    WebPushDeviceSerializer,
)


class WebPushDeviceViewSet(ModelViewSet):
    model = WebPushDevice
    permission_classes = (IsAuthenticated, IsOwner)
    serializer_class = WebPushDeviceSerializer
    parser_classes = (JSONParser, )

    def get_queryset(self):
        if getattr(self, 'swagger_fake_view', False):
            # Queryset for schema generation metadata
            return self.model.objects.none()
        try:
            return self.model.objects.filter(user=self.request.user)
        except:
            raise Http404()
    
    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
