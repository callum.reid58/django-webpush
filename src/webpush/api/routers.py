# Import external packages
from rest_framework.routers import SimpleRouter

# Import my packages
from webpush.api.viewsets import (
    WebPushDeviceViewSet,
)

# Create router
router = SimpleRouter()

# Add routes
router.register(
    r'subscriptions',
    WebPushDeviceViewSet,
    basename='subscriptions')